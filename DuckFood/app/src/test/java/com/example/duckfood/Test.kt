import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavHostController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.model.Clientes
import com.example.duckfood.model.Comidas
import com.example.duckfood.model.Pedidos
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertEquals
import org.junit.Test
import io.mockk.verify

class ViewModelTest {

    @Test
    fun testProductosLoaded() {
        // Crea una instancia del ViewModel utilizando un objeto falso o de prueba
        val viewModel = ViewModel()

        // Agrega datos a viewModel.cartProducts para simular la carga de datos en la lista
        val fakeData = listOf(
            Comidas("Pizza", "Comida rápida", "Deliciosa pizza", 9.99, "https://example.com/pizza"),
            Comidas("Hamburguesa", "Comida rápida", "Sabrosa hamburguesa", 8.99, "https://example.com/hamburguesa")
        )
        viewModel.setCartProducts(fakeData.toMutableList())

        // Accede a la variable productos y verifica que contenga los datos esperados
        val productos = viewModel.cartProducts
        assertEquals(fakeData, productos)
    }


    @Test
    fun testAñadirPedidoActual() {
        // Crea una instancia del ViewModel utilizando un objeto falso o de prueba
        val viewModel = mockk<ViewModel>()

        // Configura el comportamiento esperado para cartProducts
        val fakeCartProducts = mutableListOf<Comidas>()
        every { viewModel.cartProducts } returns fakeCartProducts

        // Configura el comportamiento esperado para pedidoActual
        val fakePedidoActual = MutableLiveData<Pedidos>()
        every { viewModel.pedidoActual } returns fakePedidoActual

        // Configura el comportamiento esperado para user
        val fakeUser = Clientes("Ivan","Gascueña","Angelillo","ivan@gmail.com","654620355")

        // Configura el comportamiento esperado para navigate
        val navController = mockk<NavHostController>(relaxed = true)

        // Simula el valor de entrada para food
        val food = Comidas("Pizza", "Comida rápida", "Deliciosa pizza", 9.99, "https://example.com/pizza")

        // Establece un valor inicial para pedidoActual
        val initialPedidoActual = Pedidos()
        fakePedidoActual.value = initialPedidoActual

        // Establece un valor inicial para user
        val initialUser =fakeUser

        // Ejecuta la función a probar
        viewModel.añadirPedidoActual(food, navController)

        // Verifica que se haya agregado food a cartProducts
        verify { fakeCartProducts.add(food) }

        // Verifica que se haya actualizado el importe de pedidoActual correctamente
        val expectedImporte = initialPedidoActual.importe + food.precio
        assertEquals(expectedImporte, fakePedidoActual.value?.importe)

        // Verifica que se haya actualizado la dirección de entrega de pedidoActual correctamente
        assertEquals(initialUser.direccion, fakePedidoActual.value?.direccionEntrega)

        // Verifica que se haya llamado a navigate con el argumento correcto
        verify { navController.navigate("MainScreen") }
    }

}
