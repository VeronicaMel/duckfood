package com.example.duckfood.screens

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.FloatingActionButtonDefaults.elevation
import androidx.compose.material.SnackbarDefaults.backgroundColor
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.PlaceholderVerticalAlign.Companion.Top
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.example.duckfood.R
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.orange
import com.example.duckfood.ui.theme.naranja
import kotlinx.coroutines.launch
import okhttp3.Route

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreen(viewModel: ViewModel, navController: NavHostController) {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    val mContext = LocalContext.current

    MainScreenBodyContent(viewModel = viewModel, navController = navController)
}

@Composable
fun scaffoldContent(text: String, icono: ImageVector, navController: NavHostController, route: String?, viewModel: ViewModel) {
    Row(modifier = Modifier.padding( top = 10.dp, bottom = 10.dp)){
        Column(modifier = Modifier .fillMaxWidth(0.05f)) {}
        Button(
            onClick = { if (route != ""){
                if (route != null) {
                    if(route.equals("OrderScreen") && viewModel.cartProducts.isEmpty()){}
                    else { navController.navigate(route)}
                } else {}
            }
            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .fillMaxWidth(0.95f)
                .padding(top = 12.dp, bottom = 12.dp),
            shape = RoundedCornerShape(100.dp)
        ) {
            Icon(
                imageVector = icono,
                contentDescription = "Menu",
                modifier = Modifier.padding(end = 10.dp)
            )
            Text(text = text, fontWeight = FontWeight.Bold)
        }
    }
    Divider(thickness = 2.dp, color = naranja,
        modifier = Modifier
            .padding(start = 20.dp, end = 20.dp, top = 10.dp)
    )
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreenBodyContent(viewModel: ViewModel,navController: NavHostController){
    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    Scaffold(
        scaffoldState = scaffoldState,
        drawerContent = {
            Column(  modifier = Modifier
                .background(fondo)
                .fillMaxSize()){
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
                    .background(color = naranja),){ Text(
                    text = "DuckFood", modifier = Modifier.padding(start = 10.dp, top = 10.dp),
                    fontSize = 20.sp,
                    color = Color.White,
                    fontWeight = FontWeight.Bold
                )}
                scaffoldContent("Inicio", Icons.Default.Home, navController, "MainScreen", viewModel)
                scaffoldContent("Perfil",Icons.Default.Info, navController, "ProfileScreen", viewModel)
                scaffoldContent("Carrito", Icons.Default.ShoppingCart, navController, "OrderScreen", viewModel)
                scaffoldContent("Historial de Pedidos", Icons.Default.Favorite, navController, "HistoryScreen", viewModel)
                scaffoldContent("Cerrar", Icons.Default.Close, navController, "MainScreen", viewModel)
            }
        }
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .background(fondo)
                .fillMaxSize()

        ) {
            Row() {
                Column(modifier = Modifier
                    .fillMaxWidth(0.2f)
                    .padding(start = 10.dp)) {
                    Button(
                        onClick = {  scope.launch {
                            scaffoldState.drawerState.apply {
                                if (isClosed) open() else close()
                            }
                        } },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = naranja,
                            contentColor = Color.White,
                        ),
                        border = BorderStroke(2.dp, color = naranja),
                        modifier = Modifier
                            .height(70.dp)
                            .width(70.dp)
                            .padding(top = 24.dp, end = 14.dp),
                        shape = RoundedCornerShape(100.dp)
                    ) {
                        Icon(imageVector = Icons.Default.Menu, contentDescription = "Menú")
                    }
                }
                Column(modifier = Modifier.fillMaxWidth(0.75f)) {
                    Image(
                        painter = painterResource(id = R.drawable.duckfoodv2),
                        contentDescription = "LogoDuckFood",
                        modifier = Modifier
                            .padding(top = 80.dp)
                            .align(Alignment.CenterHorizontally)
                    )
                }
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 10.dp)) {
                    if(viewModel.cartProducts.isNotEmpty()) {
                        Button(
                            onClick = { navController.navigate("OrderScreen") },
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = naranja,
                                contentColor = Color.White,
                            ),
                            border = BorderStroke(2.dp, color = naranja),
                            modifier = Modifier
                                .height(70.dp)
                                .width(70.dp)
                                .padding(top = 24.dp, start = 14.dp),
                            shape = RoundedCornerShape(100.dp)
                        ) {
                            Icon(
                                imageVector = Icons.Default.ShoppingCart,
                                contentDescription = "Carrito"
                            )
                        }
                    } else {}
                }
            }

            Spacer(modifier = Modifier.size(7.dp))

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .horizontalScroll(rememberScrollState())
            ) {
                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Entrantes",
                    icon = painterResource(id = R.drawable.entrantes),
                    category = "Entrantes",
                    viewModel
                )

                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Hamburguesas",
                    icon = painterResource(id = R.drawable.borger),
                    category = "Hamburguesas",
                    viewModel
                )

                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Pizzas",
                    icon = painterResource(id = R.drawable.pizza),
                    category = "Pizzas",
                    viewModel
                )

                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Perritos",
                    icon = painterResource(id = R.drawable.perritos),
                    category = "Perritos",
                    viewModel
                )

                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Tacos",
                    icon = painterResource(id = R.drawable.tacos),
                    category = "Tacos",
                    viewModel
                )

                Spacer(modifier = Modifier.size(20.dp))

                CategoryButton(
                    text = "Postres",
                    icon = painterResource(id = R.drawable.helao),
                    category = "Postres",
                    viewModel
                )

                Spacer(modifier = Modifier.size(24.dp))

            }
            Spacer(modifier = Modifier.size(24.dp))

            showFoodsByCategory(viewModel = viewModel, navController = navController)
        }
    }
    }

@Composable
fun CategoryButton(
    text: String,
    icon: Painter,
    category: String,
    viewModel: ViewModel
) {
    Column(
        modifier = Modifier
            .width(130.dp)
            .clickable {
                viewModel.retrieveFoodByCategory(category)
            }
    ){
        Box(
            modifier = Modifier
                .size(100.dp)
                .padding(1.dp)
                .align(CenterHorizontally)
        ){
            Image(painter = icon, contentDescription = "",
                Modifier
                    .width(250.dp)
                    .height(250.dp)
                    .align(Alignment.Center))
        }
        Text(text = text, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center, color = naranja, fontWeight = FontWeight.Bold)

    }
}

@Composable
fun showFoodsByCategory(viewModel: ViewModel,navController: NavHostController) {

        var foodList = viewModel.comidasCategoryList
        if (foodList.isNotEmpty()) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 8.dp, start = 16.dp, end = 16.dp)
            ) {
                items(foodList) {
                    //println(it)
                    Card(
                        modifier = Modifier
                            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                            .clickable {
                                viewModel.setFood(it)
                                navController.navigate("FoodScreen")
                            },
                        elevation = 16.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = Color(0xFFFFF2B0)
                    ) {
                        Row(
                            modifier = Modifier
                                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp, top = 16.dp)
                                .fillMaxWidth(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            FirebaseImage(url = it.url)

                            Spacer(modifier = Modifier.width(32.dp))

                            Text(
                                modifier = Modifier
                                    .align(Alignment.CenterVertically),
                                text = it.nombre,
                                fontSize = 24.sp,
                                color = naranja
                            )
                        }
                    }
                }
            }
        } else {

        }
    }

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}

@Composable
fun FirebaseImage(url: String) {
    val painter = rememberAsyncImagePainter(url)
    Image(
        painter = painter,
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(100.dp, 100.dp)
            .padding(8.dp)
            .border(width = 2.dp, color = naranja),
        alignment = Center
    )
}

