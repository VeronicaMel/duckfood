package com.example.practicafirebase.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.*

//import com.example.practicafirebase.agregar.ViewModel.CreateViewModel
//import com.example.practicafirebase.screens.*

@Composable
fun AppNavigation(ViewModel: ViewModel) {

    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = AppScreens.SplashScreen.route)
    {
        composable(route = AppScreens.SplashScreen.route) {SplashScreen(navController)}
        composable(route = AppScreens.FirstActivity.route) {FirstActivity(ViewModel, navController)}
        composable(route = AppScreens.LoginActivity.route) {LoginActivity(ViewModel, navController)}
        composable(route = AppScreens.RegisterActivity.route) { RegisterActivity(ViewModel, navController) }
        composable(route = AppScreens.MainScreen.route) { MainScreen(ViewModel, navController) }
        composable(route = AppScreens.FoodScreen.route) { FoodScreen(ViewModel, navController) }
        composable(route = AppScreens.ProfileScreen.route){ ProfileScreen(ViewModel, navController)}
        composable(route = AppScreens.OrderScreen.route) { OrderScreen(ViewModel, navController) }
        composable(route = AppScreens.PaymentScreen.route) { PaymentScreen(ViewModel, navController) }
        composable(route = AppScreens.ConfirmationScreen.route) {ConfirmationScreen(ViewModel, navController)}
        composable(route = AppScreens.HistoryScreen.route) {HistoryScreen(ViewModel, navController)}
    }
}