package com.example.duckfood.screens

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.model.Comidas
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.orange
import com.example.duckfood.ui.theme.naranja


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun OrderScreen(viewModel: ViewModel, navController: NavHostController) {

    OrderScreenBodyContent(viewModel, navController = navController)

}

@Composable
fun OrderScreenBodyContent(viewModel: ViewModel,navController: NavHostController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(fondo)
            .fillMaxSize()

    ) {
        Spacer(modifier = Modifier.size(24.dp))
        Card(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth()
                .fillMaxHeight(0.9f),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color(0xFFFFF2B0)
        ) {

            Column(modifier = Modifier
                .padding(bottom = 24.dp),
                horizontalAlignment = CenterHorizontally
            )
            {
                Spacer(modifier = Modifier.size(25.dp))
                // 2
                print((viewModel.cartProducts))
                showProducts(viewModel = viewModel, navController = navController )
            }
        }
        Spacer(modifier = Modifier.size(7.dp))
       payRow(navController = navController, viewModel = viewModel)
    }
}

@Composable
fun showProducts(viewModel: ViewModel,navController: NavHostController) {
    //var productos = viewModel.cartProducts
    var productos: MutableList<Comidas> = viewModel.cartProducts
    var productosFilter = productos.distinctBy { it.nombre }
    println(productosFilter)
        if (productos.isNotEmpty()) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 8.dp, start = 16.dp, end = 16.dp)
            ) {
                items(productosFilter) {
                    orderCard(navController = navController,viewModel, nombre = it.nombre, precio = it.precio, producto = it, productos )
                }
            }
        } else
        { }
}



@Composable
fun orderCard(
    navController: NavHostController,
    viewModel: ViewModel,
    nombre: String,
    precio: Double,
    producto: Comidas,
    productos: MutableList<Comidas>
){
    Row(
        modifier = Modifier
            .padding(start = 6.dp, end= 6.dp),
        verticalAlignment = Alignment.CenterVertically
    ){
        Card(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .size(width = 340.dp, height = 130.dp),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color.White
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier
                    .fillMaxHeight(0.5f)
                    .fillMaxWidth()) {
                    Column(modifier = Modifier
                        .fillMaxWidth(0.7f)
                        .fillMaxHeight(1f)) {
                        Text(text = nombre, textAlign = TextAlign.Start, color = naranja, fontSize = 23.sp, fontWeight = FontWeight.Bold, modifier = Modifier
                            .weight(2f)
                            .padding(start = 15.dp, top = 4.dp))
                    }
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)) {
                        Text(text ="" + productos.count { it.nombre == producto.nombre } , fontWeight = FontWeight.Bold, fontSize = 25.sp, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth(), color = orange)
                    }

                }
                Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                ) {
                    Column(modifier = Modifier
                        .fillMaxWidth(0.4f)
                        .fillMaxHeight()) {
                        Text(text = "Precio:\n ${precio * productos.count { it.nombre == producto.nombre }} €", textAlign = TextAlign.Start, color = naranja, fontSize = 20.sp, fontWeight = FontWeight.Bold, modifier = Modifier
                            .weight(1f)
                            .padding(start = 10.dp, bottom = 10.dp))
                    }
                    Spacer(modifier = Modifier.size(40.dp))
                    Column(modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight()) {
                        Row(modifier = Modifier.fillMaxSize()){
                            Button(
                                onClick = {
                            /*navController.navigate("MainScreen")*/
                                viewModel.addToCardProducts(producto)
                                    Log.d("tag", "${viewModel.cartProducts.size}" )
                                    navController.navigate("OrderScreen")
                                },

                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = naranja  ,
                                    contentColor = Color.White,
                                ),
                                border = BorderStroke(2.dp, color = naranja),
                                modifier = Modifier
                                    .height(50.dp)
                                    .width(50.dp),
                                shape = RoundedCornerShape(100.dp)){
                                Text(text = "+", fontSize = 25.sp, fontWeight = FontWeight.Bold)
                            }
                            Spacer(modifier = Modifier.size(10.dp))
                            Button(onClick = {
                                viewModel.removeCardProduct(producto)
                                Log.d("tag", "${viewModel.cartProducts.size}" )
                                if(viewModel.cartProducts.isEmpty()){
                                    navController.navigate("MainScreen")
                                } else {
                                navController.navigate("OrderScreen")
                                } },
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = naranja  ,
                                    contentColor = Color.White,
                                ),
                                border = BorderStroke(2.dp, color = naranja),
                                modifier = Modifier
                                    .height(50.dp)
                                    .width(50.dp),
                                shape = RoundedCornerShape(100.dp)){
                                Text(text = "-", fontSize = 25.sp, fontWeight = FontWeight.Bold)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun payRow(navController: NavHostController, viewModel: ViewModel){

    Row(modifier = Modifier
        .fillMaxHeight()
        .fillMaxWidth()){
        Column(modifier = Modifier.fillMaxWidth(0.080f)){}
        Button(onClick = { navController.navigate("MainScreen") },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .fillMaxHeight(0.8f)
                .fillMaxWidth(0.2f)
                .padding(end = 10.dp),
            shape = RoundedCornerShape(100.dp)){
            Icon(imageVector = Icons.Default.ArrowBack , contentDescription = "Retroceder" )
        }
        Button(onClick = {
            /*viewModel.saveOrderInDatabase(viewModel.pedidoActual.value!!.direccionEntrega, viewModel.pedidoActual.value!!.importe )*/
            navController.navigate("PaymentScreen") },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .fillMaxHeight(0.8f)
                .fillMaxWidth(0.9f),
            shape = RoundedCornerShape(100.dp)){
            Text(text = "Pagar", fontSize = 25.sp, fontWeight = FontWeight.Bold)
        }
    }
}









