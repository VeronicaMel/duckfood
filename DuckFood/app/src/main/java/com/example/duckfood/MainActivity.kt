package com.example.duckfood

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.duckfood.ViewModel.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.duckfood.ui.theme.DuckFoodTheme
import com.example.practicafirebase.navigation.AppNavigation

class MainActivity : ComponentActivity() {
    val viewModel by viewModels<ViewModel> ()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DuckFoodTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),

                ) {
                    AppNavigation(viewModel)
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DuckFoodTheme {
        AppNavigation(viewModel())
    }
}