package com.example.duckfood.screens

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.ui.theme.naranja
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.duckfood.R
import com.example.duckfood.screens.ui.theme.yellow
import kotlinx.coroutines.launch

@Composable
fun ProfileScreen(viewModel: ViewModel, navController: NavHostController) {
    ProfileScreenContent(viewModel, navController)
}

@Composable
fun ProfileScreenContent(viewModel: ViewModel, navController: NavHostController) {
    val email = viewModel.auth.currentUser?.email
    var dato by remember { mutableStateOf("") }
    var showTextField by remember { mutableStateOf(false) }

    if (email != null) {


        var currentUser by remember { mutableStateOf(viewModel._user.value) }
        Log.d("tag", "el cliente no llega bien $currentUser")
        if (currentUser != null) {
            LazyColumn(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .background(fondo)
                    .fillMaxSize()
            ) {
                item {
                    Row(modifier = Modifier.padding(bottom = 16.dp)) {
                        Column(modifier = Modifier.weight(1f)) {
                            Button(
                                onClick = { navController.navigate("MainScreen") },
                                colors = ButtonDefaults.buttonColors(
                                    backgroundColor = naranja,
                                    contentColor = Color.White,
                                ),
                                border = BorderStroke(2.dp, color = naranja),
                                modifier = Modifier
                                    .height(70.dp)
                                    .width(70.dp)
                                    .padding(top = 24.dp, start = 14.dp),
                                shape = RoundedCornerShape(100.dp)
                            ) {
                                Icon(
                                    imageVector = Icons.Default.ArrowBack,
                                    contentDescription = "Volver"
                                )
                            }

                        }
                        Column(modifier = Modifier.weight(3f)) {
                            Image(
                                painter = painterResource(id = R.drawable.duckfoodv2),
                                contentDescription = "LogoDuckFood",
                                modifier = Modifier
                                    .padding(top = 60.dp)
                                    .align(Alignment.CenterHorizontally)
                            )
                        }
                        Column(modifier = Modifier.weight(1f)) {
                        }
                    }
                    Spacer(modifier = Modifier.size(16.dp))
                    Card(
                        modifier = Modifier
                            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                            .height(100.dp),
                        elevation = 16.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = Color(0xFFFFF2B0)
                    ) {

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 16.dp)
                        ) {

                            Text(
                                text = "Nombre:",
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h3,
                                modifier = Modifier.weight(2f)
                            )
                            Spacer(modifier = Modifier.size(8.dp))
                            Text(
                                text = currentUser!!.nombre + " " + currentUser!!.apellidos,
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h2,
                                modifier = Modifier.weight(2f)
                            )
                        }
                    }

                    Spacer(modifier = Modifier.size(16.dp))

                    Card(
                        modifier = Modifier
                            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                            .height(100.dp),
                        elevation = 16.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = Color(0xFFFFF2B0)
                    )
                    {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 16.dp)
                        ) {
                            Text(
                                text = "Direccion:",
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h3,
                                modifier = Modifier.weight(2f)
                            )
                            Spacer(modifier = Modifier.size(8.dp))
                            Text(
                                text = currentUser!!.direccion,
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h2,
                                modifier = Modifier.weight(2f)
                            )

                        }
                    }
                    Spacer(modifier = Modifier.size(16.dp))
                    Card(
                        modifier = Modifier
                            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                            .height(100.dp),
                        elevation = 16.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = Color(0xFFFFF2B0)
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 16.dp)
                        ) {
                            Text(
                                text = "Email:",
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h3,
                                modifier = Modifier.weight(2f)
                            )
                            Spacer(modifier = Modifier.size(8.dp))
                            Text(
                                text = currentUser!!.email,
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h2,
                                modifier = Modifier.weight(2f)
                            )

                        }
                    }
                    Spacer(modifier = Modifier.size(16.dp))

                    Card(
                        modifier = Modifier
                            .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                            .height(100.dp),
                        elevation = 16.dp,
                        shape = RoundedCornerShape(8.dp),
                        backgroundColor = Color(0xFFFFF2B0)
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 16.dp)
                        ) {
                            Text(
                                text = "Teléfono:",
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h3,
                                modifier = Modifier.weight(2f)
                            )
                            Spacer(modifier = Modifier.size(8.dp))
                            Text(
                                text = currentUser!!.telefono,
                                textAlign = TextAlign.Start,
                                color = naranja,
                                style = MaterialTheme.typography.h2,
                                modifier = Modifier.weight(2f)
                            )

                        }
                    }
                    Spacer(modifier = Modifier.size(24.dp))

                    Column(
                        modifier = Modifier.padding(start = 16.dp, end = 16.dp)
                    ) {
                        if (showTextField) {
                            TextFieldUpdate(
                                dato = dato,
                                email = email,
                                viewModel = viewModel,
                                navController
                            )
                        } else {
                            Row(modifier = Modifier.align(alignment = CenterHorizontally)) {
                                Button(
                                    onClick = { dato = "Direccion"; showTextField = true },
                                    colors = ButtonDefaults.buttonColors(
                                        backgroundColor = naranja,
                                        contentColor = Color.White,
                                    ),
                                    border = BorderStroke(2.dp, color = naranja),
                                    modifier = Modifier
                                        .height(40.dp)
                                        .width(280.dp)
                                        .align(CenterVertically),
                                    shape = RoundedCornerShape(100.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Home,
                                        contentDescription = "Menu",
                                        modifier = Modifier
                                            .padding(end = 10.dp)
                                            .align(CenterVertically)
                                    )
                                    Text(text = "Modificar dirección", fontWeight = FontWeight.Bold)
                                }
                            }
                            Spacer(modifier = Modifier.size(24.dp))
                            Row(modifier = Modifier.align(alignment = CenterHorizontally)) {
                                Button(
                                    onClick = {
                                        dato = "Telefono"; showTextField = true
                                    },
                                    colors = ButtonDefaults.buttonColors(
                                        backgroundColor = naranja,
                                        contentColor = Color.White,
                                    ),
                                    border = BorderStroke(2.dp, color = naranja),
                                    modifier = Modifier
                                        .height(40.dp)
                                        .width(280.dp)
                                        .align(CenterVertically),
                                    shape = RoundedCornerShape(100.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.Phone,
                                        contentDescription = "Menu",
                                        modifier = Modifier
                                            .padding(end = 10.dp)
                                            .align(CenterVertically)
                                    )
                                    Text(text = "Modificar teléfono", fontWeight = FontWeight.Bold)
                                }
                            }
                        }

                    }
                    Spacer(modifier = Modifier.size(16.dp))

                }
            }
        }
    }
}

@Composable
fun TextFieldUpdate(
    dato: String,
    email: String,
    viewModel: ViewModel,
    navController: NavHostController,
) {
    val mContext = LocalContext.current
    var text = ""
    if (dato == "Direccion") {
        text = "Introduce la nueva dirección"
    } else {
        text = "Introduce el nuevo teléfono"
    }
    var updateString by remember { mutableStateOf("") }
    Column() {
        OutlinedTextField(
            value = updateString,
            textStyle = LocalTextStyle.current.copy(color = Color.Black),
            onValueChange = {
                updateString = it
            },
            label = {
                Text(
                    text,
                    color = naranja,
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = naranja,
                unfocusedBorderColor = yellow
            ),
            modifier = Modifier
                .fillMaxWidth()
                .align(CenterHorizontally)
                .padding(start = 16.dp, end = 16.dp),
            singleLine = true,
        )
        Spacer(modifier = Modifier.size(24.dp))
        Button(
            onClick = {
                if (dato == "Direccion") {
                    viewModel.updateProfile(email, "Direccion", updateString, navController)
                    viewModel.pedidoActual.value!!.direccionEntrega = updateString
                } else {
                    try {
                        if(updateString.toInt() in 100000000..999999999) {
                    viewModel.updateProfile(email, "Telefono", updateString, navController)
                } else {  mToast(mContext, "Introduce un teléfono válido")}
            } catch (e: java.lang.NumberFormatException) {  mToast(mContext, "Introduce un teléfono válido")}
                }

            },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .height(40.dp)
                .width(200.dp)
                .align(CenterHorizontally),
            shape = RoundedCornerShape(100.dp)
        ) {
            Icon(
                imageVector = Icons.Default.Edit,
                contentDescription = "Menu",
                modifier = Modifier.padding(end = 10.dp)
            )
            Text(text = "Actualizar", fontWeight = FontWeight.Bold)
        }
        Spacer(modifier = Modifier.size(16.dp))
    }
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}












