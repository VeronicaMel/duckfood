package com.example.duckfood.screens

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.duckfood.R
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.black
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.yellow
import com.example.duckfood.ui.theme.naranja
import com.example.practicafirebase.navigation.AppScreens
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth

@Composable
fun RegisterActivity (viewModel: ViewModel, navController: NavHostController) {

    RegisterActivityBodyContent(viewModel, navController = navController)

}

@Composable
fun RegisterActivityBodyContent(viewModel: ViewModel, navController: NavHostController) {
    val fb = FirebaseAuth.getInstance()
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var name by remember { mutableStateOf("") }
    var surname by remember { mutableStateOf("") }
    var address by remember { mutableStateOf("") }
    var phone by remember { mutableStateOf("") }
    val dato = hashMapOf<String, String>()

    val mContext = LocalContext.current


    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .fillMaxSize()

    ) {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .background(fondo)
                .fillMaxSize()

        ) {

            Image(
                painter = painterResource(id = R.drawable.duckfoodv2),
                contentDescription = "LogoDuckFood",
                modifier = Modifier
                    .padding(top = 24.dp, bottom = 24.dp)
                    .align(Alignment.CenterHorizontally)
            )

            OutlinedTextField(
                value = email,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    email = it
                },
                label = {
                    Text(
                        "Introduce el email",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = password,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = { password = it
                },
                label = {
                    Text(
                        "Introduce una contraseña",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = name,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    name = it
                },
                label = {
                    Text(
                        "Introduce el nombre",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = surname,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    surname = it
                },
                label = {
                    Text(
                        "Introduce los apellidos",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = address,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    address = it
                },
                label = {
                    Text(
                        "Introduce la direccion completa",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = phone,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    phone = it
                },
                label = {
                    Text(
                        "Introduce el telefono",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            Button(
                onClick = {
                    if(email!=""||password!=""||name!=""||surname!=""||address!=""||phone!="") {
                        try {
                            if(phone.toInt() in 100000000..999999999) {
                                if(password.length>=6){
                                viewModel.registerUser(email, password, name, surname, address, phone, mContext, navController)
                                    }
                            else {  mToast(mContext, "La contraseña debe tener por lo menos 6 digitos")}
                            } else {
                                mToast(mContext, "Introduce un teléfono válido")
                            }
                        } catch (e: java.lang.NumberFormatException) { mToast(mContext, "Introduce un teléfono válido")}

                    } else{
                        mToast(mContext, "Debes rellenar todos los campos correctamente")
                    }
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = naranja,
                    contentColor = Color.White,
                ),
                border = BorderStroke(2.dp, color = naranja),
                modifier = Modifier
                    .height(40.dp)
                    .width(280.dp)
                    .align(Alignment.CenterHorizontally),
                shape = RoundedCornerShape(100.dp)
            ) {
                Text(
                    text = "Guardar",
                    style = MaterialTheme.typography.button,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }
            Spacer(modifier = Modifier.size(5.dp))
            Row(modifier = Modifier.fillMaxWidth().height(400.dp)){}
        }

    }
}

private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}
