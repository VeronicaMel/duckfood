package com.example.duckfood.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.ui.theme.naranja


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HistoryScreen(viewModel: ViewModel, navController: NavHostController) {

    HistoryScreenBodyContent(viewModel, navController = navController)

}

@Composable
fun HistoryScreenBodyContent(viewModel: ViewModel,navController: NavHostController) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(fondo)
            .fillMaxSize()

    ) {
        Card(
            modifier = Modifier
                .padding(bottom = 18.dp, start = 16.dp, end = 16.dp, top = 16.dp)
                .fillMaxWidth()
                .fillMaxHeight(0.90f)
                .verticalScroll(rememberScrollState()),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color(0xFFFFF2B0)
        ) {

            Column(modifier = Modifier
                .padding(bottom = 24.dp),
                horizontalAlignment = CenterHorizontally
            )
            {
                Spacer(modifier = Modifier.size(10.dp))
                // 2
                if(viewModel.historial!!.isNotEmpty()) {
                    showOrders(viewModel = viewModel, navController = navController)
                }
                else {
                    Text(text = "Cuanto vacío...", color = Color.Black, style = MaterialTheme.typography.h3 )
                    Text(text = "¿Por qué no pruebas a pedir algo?", color = Color.Black, style = MaterialTheme.typography.h3 )
                }
            }
        }
        backRow(navController = navController, viewModel = viewModel )
    }
}

@Composable
fun backRow(navController: NavHostController, viewModel: ViewModel){

    Row(){
        Button(onClick = { navController.navigate("MainScreen") },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .height(60.dp)
                .width(200.dp),
            shape = RoundedCornerShape(100.dp)){
            Icon(imageVector = Icons.Default.ArrowBack , contentDescription = "Retroceder" )
            Text(text = " Volver", fontSize = 25.sp, fontWeight = FontWeight.Bold)
        }
    }
}

@Composable
fun showOrders(
    navController: NavHostController,
    viewModel: ViewModel
){
        for(elementos in viewModel.historial!!) {
            println(elementos)
            Row(
                modifier = Modifier
                    .padding(start = 6.dp, end = 6.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Card(
                    modifier = Modifier
                        .padding(bottom = 16.dp, start = 16.dp, end = 16.dp, top = 16.dp),
                    elevation = 16.dp,
                    shape = RoundedCornerShape(8.dp),
                    backgroundColor = Color.White
                ) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        Row(
                            modifier = Modifier
                                .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 5.dp)
                        ) {
                            Column() {
                                Text(text = "Fecha: ", fontSize = 20.sp, fontWeight = FontWeight.Bold, color = naranja)
                                Text(text = elementos.fecha, color = Color.Black)
                            }
                        }
                        Spacer(modifier = Modifier.size(0.dp))
                        Row(
                            modifier = Modifier
                                .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 5.dp)
                        ) {
                            Column() {
                                Text(text = "Importe: ", fontSize = 20.sp, fontWeight = FontWeight.Bold, color = naranja)
                                Text(text = elementos.importe.toString()+"€", color = Color.Black)
                            }
                        }
                        Spacer(modifier = Modifier.size(0.dp))
                        Row(
                            modifier = Modifier
                                .padding(start = 5.dp, end = 5.dp, top = 5.dp, bottom = 5.dp)
                        ) {
                            Column() {
                                Text(text = "Dirección: ", fontSize = 20.sp, fontWeight = FontWeight.Bold, color = naranja)
                                Text(text = elementos.direccionEntrega, color = Color.Black)
                            }
                        }
                    }
                }
            }
        }
    }













