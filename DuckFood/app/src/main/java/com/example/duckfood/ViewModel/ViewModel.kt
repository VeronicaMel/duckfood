package com.example.duckfood.ViewModel

import android.content.Context
import android.util.Log
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import com.example.duckfood.model.Clientes
import com.example.duckfood.model.Comidas
import com.example.duckfood.model.Pedidos
import com.example.duckfood.model.MainState
import com.example.practicafirebase.navigation.AppScreens
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.util.Calendar

class ViewModel : ViewModel() {

    var mail = MutableLiveData<String>()
    var name = MutableLiveData<String>()
    val dato = hashMapOf<String, String>()
    var _user = MutableLiveData<Clientes>()

    var success = MutableLiveData<Boolean?>()
    var message = MutableLiveData<String>()
    var comidasCategoryList: MutableList<Comidas> by mutableStateOf(mutableListOf())
    var selectedFood = MutableLiveData<Comidas>()
    var pedidoActual = MutableLiveData<Pedidos>(Pedidos())
    var cartProducts: MutableList<Comidas> by mutableStateOf(mutableListOf())
    val auth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    var historial: ArrayList<Pedidos>? = ArrayList()


    fun addToCardProducts(producto: Comidas){
        cartProducts.add(producto)
        pedidoActual.value!!.importe =   (pedidoActual.value!!.importe.toDouble() + producto.precio)
    }

    fun removeCardProduct(producto: Comidas){
        cartProducts.remove(producto)
        pedidoActual.value!!.importe =   (pedidoActual.value!!.importe.toDouble() - producto.precio)
    }

    fun añadirPedidoActual(food: Comidas, navController: NavHostController){
        cartProducts.add(food)
        pedidoActual.value!!.importe =   pedidoActual.value!!.importe + food.precio
        pedidoActual.value!!.direccionEntrega = _user.value!!.direccion
        navController.navigate("MainScreen")
    }

    //Registra el usuario en Autenticación de Firebase. Si la autenticación se realiza con éxito, guarda el usuario con toda
    // su información en la colección de Clientes
    fun registerUser(
        email: String,
        password: String,
        nombre: String,
        apellidos: String,
        direccion: String,
        telefono: String,
        context: Context,
        navController: NavHostController
    ) {
        if (email.isNotEmpty() && password.isNotEmpty()) {
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    success.value = true
                    if (it.isSuccessful) {
                        saveUserInDatabase(email, password, nombre, apellidos, direccion, telefono)
                        message.postValue("Login realizado con éxito")
                        navController.navigate(AppScreens.FirstActivity.route)
                    } else {
                        success.value = false
                    }
                }
        }
    }

    // Crea un usuario con los datos introducidos en la coleccion Clientes de la base de datos
    fun saveUserInDatabase(
        email: String,
        password: String,
        nombre: String,
        apellidos: String,
        direccion: String,
        telefono: String
    ) {
        val nombre_coleccion = "Clientes"
        dato["Nombre"] = nombre
        dato["Apellidos"] = apellidos
        dato["Telefono"] = telefono
        dato["Email"] = email
        dato["Direccion"] = direccion

        db.collection(nombre_coleccion)
            .document(email)
            .set(dato)
            .addOnSuccessListener {
                success.postValue(true)
            }

            .addOnFailureListener {
                success.postValue(false)
            }
    }

    fun saveOrderInDatabase(
        direccionEntrega: String,
        importe: Double,
    ) {
        var currentDate = Calendar.getInstance().time
        val nombre_coleccion = "Pedidos"
        dato["Email"] = mail.value.toString()
        dato["Fecha"] = currentDate.toString()
        dato["Importe"] = importe.toString()
        dato["Direccion"] = direccionEntrega
        println(dato)
        db.collection(nombre_coleccion)
            .add(dato)
            .addOnSuccessListener {
                success.postValue(true)
                cartProducts.clear()
                pedidoActual.value!!.importe = 0.0
                pedidoActual.value!!.direccionEntrega = ""
            }

            .addOnFailureListener {
                success.postValue(false)
            }
    }

    fun searchClientInDatabase(email: String): Clientes? {
        val db = FirebaseFirestore.getInstance()
        var cliente: Clientes? = null
        viewModelScope.launch {
            db.collection("Clientes").whereEqualTo("Email", email).get()
                .addOnSuccessListener { result ->
                    if (!result.isEmpty) {
                        val document = result.documents[0]
                        val name = document.getString("Nombre").toString()
                        val surname = document.getString("Apellidos").toString()
                        val address = document.getString("Direccion").toString()
                        val phone = document.getString("Telefono").toString()
                        val mail = document.getString("Email").toString()
                        cliente = Clientes(name, surname, address, mail, phone)
                        _user.postValue(cliente)
                    } else {

                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("MyTag", "Error al obtener el cliente: $email", exception)
                }
        }
        return cliente
    }

    //Funcion que controla el Login una vez que el usuario está registrado en la Autenticación de Firebase.
    // Si el login es correcto, asigna el email correspondiente a la variable mail y buscar al usuario en
    // la coleccion de clientes para guardarlo en la variable user
    fun userAcces(email: String, password: String, navController: NavHostController) {
        viewModelScope.launch {
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if (auth.currentUser != null) {
                            mail.value = auth.currentUser!!.email
                            _user.postValue(searchClientInDatabase(mail.value!!))
                            success.postValue(true)
                        }
                        navController.navigate(AppScreens.MainScreen.route)
                    } else {
                        success.postValue(false)
                    }
                }

        }
    }

    //Recupera un listado de comidas en función de su categoría para mostrar en la pantalla principal
    fun retrieveFoodByCategory(category: String): List<Comidas> {
        val nombre_coleccion = "Comida"
        viewModelScope.launch {
            db.collection(nombre_coleccion).whereEqualTo("Categoria", category).get()
                .addOnSuccessListener { result ->
                    val list = mutableListOf<Comidas>()
                    for (document in result) {
                        val nombre = document.id.toString()
                        val categoria = document.get("Categoria").toString()
                        val descripcion = document.get("Descripcion").toString()
                        val precio = document.get("Precio").toString().toDouble()
                        val url = document.get("URL").toString()

                        var comida = Comidas(nombre, categoria, descripcion, precio, url)
                        list.add(comida)
                    }
                    comidasCategoryList = list
                }
        }
        return comidasCategoryList
    }

    // Recibe el valor de la comida seleccionada para mostrarla en la pantalla de detalle de la comida
    fun setFood(food: Comidas) {
        selectedFood.value = food
    }

    // Funcion para actualizar los datos del usuario en la base de datos
    fun updateProfile(
        email: String,
        field: String,
        date: String,
        navController: NavHostController
    ) {
        viewModelScope.launch {
            val querySnapshot = db.collection("Clientes").whereEqualTo("Email", email).get().await()

            if (!querySnapshot.isEmpty) {
                val documentSnapshot = querySnapshot.documents[0]
                //val documentId = email
                //val updatedData = hashMapOf(field to date)
                db.collection("Clientes").document(email)
                    .update(field, date).await()
                // La direccion se ha actualizado correctamente
                searchClientInDatabase(email)
                navController.navigate("MainScreen")

            } else {

            }
        }
    }

    fun searchOrdersInDatabase(email: String?) {
        val db = FirebaseFirestore.getInstance()
        var pedidosList: ArrayList<Pedidos>? = ArrayList()
        viewModelScope.launch {
            db.collection("Pedidos").whereEqualTo("Email", email).get()
                .addOnSuccessListener { result ->
                    if (!result.isEmpty) {
                        for (document in result) {
                            var email = document.getString("Email").toString()
                            var importe = document.getString("Importe")!!.toDouble()
                            var direccionEntrega = document.getString("Direccion").toString()
                            var fecha = document.getString("Fecha").toString()
                            val pedido = Pedidos(email, importe, direccionEntrega, fecha)
                            pedidosList!!.add(pedido)
                            println(pedidosList)
                        }
                        historial = pedidosList
                    } else {

                    }
                }
                .addOnFailureListener { exception ->

                }
        }
        historial = pedidosList
    }

/*    fun setCartProducts(products: MutableList<Comidas>) {
        cartProducts = products
    }
*/
}

