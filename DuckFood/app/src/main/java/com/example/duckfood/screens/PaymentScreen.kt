package com.example.duckfood.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.duckfood.R
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.yellow
import com.example.duckfood.ui.theme.naranja
import java.text.DecimalFormat


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun PaymentScreen(viewModel: ViewModel, navController: NavHostController) {
    PaymentScreenBodyContent(viewModel, navController = navController)
}

@Composable
fun PaymentScreenBodyContent(viewModel: ViewModel,navController: NavHostController) {

    var showTextField by remember { mutableStateOf(false) }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(fondo)
            .fillMaxSize()

    ) {
        Spacer(modifier = Modifier.size(24.dp))
        Row(modifier = Modifier.fillMaxWidth().fillMaxHeight(0.25f)){
            Column(modifier = Modifier.fillMaxHeight().fillMaxWidth(0.20f)){}
            Image(
            painter = painterResource(id = R.drawable.duckfoodv2),
            contentDescription = "LogoDuckFood",
            modifier = Modifier
                .padding(top = 20.dp)
        )}
        Card(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth().fillMaxHeight(0.8f),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color(0xFFFFF2B0)
        ) {

            Column(modifier = Modifier
                .padding(bottom = 24.dp),
                horizontalAlignment = CenterHorizontally
            )
            {
              Row(modifier = Modifier.fillMaxWidth().fillMaxHeight(0.05f)){}
                // 2
                Text(
                    text = "Direccion de envío:",
                    textAlign = TextAlign.Center,
                    color = naranja,
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.weight(2f)
                )
                Spacer(modifier = Modifier.size(0.dp))
                Text(
                    text = "" + viewModel.pedidoActual.value!!.direccionEntrega,
                    textAlign = TextAlign.Center,
                    color = naranja,
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.weight(2f)
                )
                Spacer(modifier = Modifier.size(0.dp))
                Text(
                    text = "Importe Total: " ,
                    textAlign = TextAlign.Start,
                    color = naranja,
                    style = MaterialTheme.typography.h2,
                    modifier = Modifier.weight(2f)
                )
                Spacer(modifier = Modifier.size(0.dp))
                Text(
                    text = "" + roundToTwoDecimals(viewModel.pedidoActual.value!!.importe) + "€",
                    textAlign = TextAlign.Center,
                    color = naranja,
                    style = MaterialTheme.typography.h1,
                    modifier = Modifier.weight(2f)
                )
                Spacer(modifier = Modifier.size(0.dp))

                Button(onClick = { showTextField = true },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = naranja  ,
                        contentColor = Color.White,
                    ),
                    border = BorderStroke(2.dp, color = naranja),
                    modifier = Modifier
                        .padding(end = 10.dp),
                    shape = RoundedCornerShape(100.dp)){
                    Icon(imageVector = Icons.Default.Home , contentDescription = "Enviar a otra dirección" )
                    Text(text = "Enviar a otra dirección", fontWeight = FontWeight.Bold)
                }
                Spacer(modifier = Modifier.size(24.dp))
                if (showTextField){
                    TextFieldAddress(navController, viewModel)
                }
            }
        }
        Spacer(modifier = Modifier.size(7.dp))
        paymentRow(navController = navController, viewModel = viewModel)
    }
}



@Composable
fun paymentRow(navController: NavHostController, viewModel: ViewModel){
    Row(){
        Button(onClick = { navController.navigate("OrderScreen") },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .height(60.dp)
                .width(70.dp)
                .padding(end = 10.dp),
            shape = RoundedCornerShape(100.dp)){
            Icon(imageVector = Icons.Default.ArrowBack , contentDescription = "Retroceder" )
        }
        Button(onClick = {
            viewModel.saveOrderInDatabase(viewModel.pedidoActual.value!!.direccionEntrega,viewModel.pedidoActual.value!!.importe.toDouble() )
            navController.navigate("ConfirmationScreen")},
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .height(60.dp)
                .width(270.dp),
            shape = RoundedCornerShape(100.dp)){
            Text(text = "Pagar", fontSize = 25.sp, fontWeight = FontWeight.Bold)
        }
    }
}

fun roundToTwoDecimals(number: Double): Double {
    val decimalFormat = DecimalFormat("#.##")
    return decimalFormat.format(number).toDouble()
}
@Composable
fun TextFieldAddress(navController: NavHostController, viewModel: ViewModel){
    var address by remember { mutableStateOf("") }
    Column() {
        OutlinedTextField(
            value = address,
            textStyle = LocalTextStyle.current.copy(color = Color.Black),
            onValueChange = {
                address = it
            },
            label = {
                Text(
                    "Introduce la dirección de envío:",
                    color = naranja,
                )
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = naranja,
                unfocusedBorderColor = yellow
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp)
                .align(CenterHorizontally),
            singleLine = true,
        )
        Spacer(modifier = Modifier.size(24.dp))
        Button(onClick = {
                   viewModel.pedidoActual.value!!.direccionEntrega = address
                    navController.navigate("PaymentScreen")
           },
            colors = ButtonDefaults.buttonColors(
                backgroundColor = naranja  ,
                contentColor = Color.White,
            ),
            border = BorderStroke(2.dp, color = naranja),
            modifier = Modifier
                .height(60.dp)
                .width(220.dp)
                .align(CenterHorizontally),
            shape = RoundedCornerShape(100.dp)){
            Text(text = "Enviar aquí", fontSize = 25.sp, fontWeight = FontWeight.Bold)
        }
    }
}








