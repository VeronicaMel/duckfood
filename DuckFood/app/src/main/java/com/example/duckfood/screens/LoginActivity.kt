package com.example.duckfood.screens

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import androidx.navigation.NavHostController
import com.example.duckfood.R
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.black
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.yellow
import com.example.duckfood.ui.theme.naranja
import com.google.firebase.auth.FirebaseAuth

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable


fun LoginActivity(viewModel: ViewModel,navController: NavHostController) {
    Scaffold (
        modifier = Modifier.fillMaxSize()
    ){
        LoginActivityBodyContent(viewModel, navController = navController)
    }


}

@Composable
fun LoginActivityBodyContent(viewModel: ViewModel, navController: NavHostController) {

    val fb = FirebaseAuth.getInstance()
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    val mContext = LocalContext.current
    var passwordVisibility by remember { mutableStateOf(false) }

    Card(
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier
            .fillMaxSize()

    ) {

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .background(fondo)
                .fillMaxSize()
        ) {
            Image(
                painter = painterResource(id = R.drawable.duckfoodv2),
                contentDescription = "LogoDuckFood",
                modifier = Modifier
                    .padding(top = 40.dp, bottom = 40.dp)
                    .align(Alignment.CenterHorizontally)
            )
            OutlinedTextField(
                value = email,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = {
                    email = it
                },
                label = {
                    Text(
                        "Introduce el email",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp),
                singleLine = true,
            )

            Spacer(modifier = Modifier.size(24.dp))

            OutlinedTextField(
                value = password,
                textStyle = LocalTextStyle.current.copy(color = Color.Black),
                onValueChange = { password = it
                },
                label = {
                    Text(
                        "Introduce una contraseña",
                        color = naranja,
                    )
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = naranja,
                    unfocusedBorderColor = yellow
                ),
                modifier = Modifier.fillMaxWidth().padding(start = 16.dp, end = 16.dp),
                singleLine = true,
                visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
                trailingIcon = {
                    val icon = if (passwordVisibility) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
                    val contentDescription = if (passwordVisibility) "Ocultar contraseña" else "Mostrar contraseña"

                    IconButton(
                        onClick = { passwordVisibility = !passwordVisibility },
                        content = {
                            Icon(
                                imageVector = icon,
                                contentDescription = contentDescription
                            )
                        }
                    )
                }
            )
            Spacer(modifier = Modifier.size(24.dp))

            Button(
                onClick = {
                    if(password!="" || email!="") {
                        viewModel.mail.value = email
                        viewModel.userAcces(email, password, navController)
                        viewModel.searchOrdersInDatabase(viewModel.mail.value)
                        if (viewModel.success.value == false) {
                            mToast(mContext, "El email o contraseña introducidos no son válidos")
                        } else {
                            mToast(mContext, "¡Bienvenido!")
                        }

                    } else{
                        mToast(mContext, "Introduce un email y contraseña válidos")
                    }
                },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = naranja,
                    contentColor = Color.White,
                ),
                border = BorderStroke(2.dp, color = naranja),
                modifier = Modifier
                    .height(40.dp)
                    .width(280.dp)
                    .align(Alignment.CenterHorizontally),
                shape = RoundedCornerShape(100.dp)
            ) {
                Text(
                    text = "Acceder",
                    style = MaterialTheme.typography.button,
                    modifier = Modifier.padding(start = 10.dp)
                )
            }

            Spacer(modifier = Modifier.size(5.dp))
        }


        }

    }


private fun mToast(context: Context, text: String){
    Toast.makeText(context, text, Toast.LENGTH_LONG).show()
}



