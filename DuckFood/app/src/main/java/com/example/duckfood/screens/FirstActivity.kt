package com.example.duckfood.screens

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.R
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.*
import com.example.duckfood.ui.theme.naranja
import com.example.practicafirebase.navigation.AppScreens

    @Composable
    fun FirstActivity(viewModel: ViewModel, navController: NavHostController) {

            FirstActivityBodyContent(viewModel, navController = navController)

    }

    @Composable
    fun FirstActivityBodyContent(viewModel: ViewModel,navController: NavHostController) {
        Card(
            elevation = 8.dp,
            shape = RoundedCornerShape(8.dp),
            modifier = Modifier
                .fillMaxSize()

        ) {

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .background(fondo)
                    .fillMaxSize()

            ) {
                Image(
                    painter = painterResource(id = com.example.duckfood.R.drawable.duckfoodv2),
                    contentDescription = "LogoDuckFood",
                    modifier = Modifier
                        .padding(top = 40.dp, bottom = 40.dp)
                        .align(Alignment.CenterHorizontally)
                )
                Text(
                    text = "¿Ya eres cliente?",
                    color = black,
                    style = MaterialTheme.typography.button,
                )
                Spacer(modifier = Modifier.size(16.dp))

                Button(
                    onClick = {
                        navController.navigate(route = AppScreens.LoginActivity.route)

                    },
                    //border = BorderStroke(2.dp, orange),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = naranja,
                        contentColor = Color.White,
                    ),
                    border = BorderStroke(2.dp, color = naranja),
                    modifier = Modifier
                        .height(40.dp)
                        .width(280.dp)
                        .align(CenterHorizontally),
                    shape = RoundedCornerShape(100.dp)

                ) {

                    Text(
                        text = "Acceder",
                        fontWeight = FontWeight.Bold
                    )
                }

                Spacer(modifier = Modifier.size(24.dp))

                Text(
                    text = "¿Eres nuevo? \n !Regístrate y disfruta de DuckFood!",
                    color = black,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.align(CenterHorizontally),
                    style = MaterialTheme.typography.button,
                )

                Spacer(modifier = Modifier.size(16.dp))

                Button(
                    onClick = {
                        navController.navigate(route = AppScreens.RegisterActivity.route)

                    },
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = naranja,
                        contentColor = Color.White,
                    ),
                    border = BorderStroke(2.dp, color = naranja),
                    modifier = Modifier
                        .height(40.dp)
                        .width(280.dp)
                        .align(CenterHorizontally),
                    shape = RoundedCornerShape(100.dp)
                ) {
                    Text(
                        text = "Registro",
                        fontWeight = FontWeight.Bold
                    )
                }

                Spacer(modifier = Modifier.size(16.dp))
            }
        }
    }
