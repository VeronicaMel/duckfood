package com.example.practicafirebase.navigation

sealed class AppScreens(val route: String) {

    object SplashScreen: AppScreens("SplashScreen")
    object FirstActivity: AppScreens("FirstActivity")
    object LoginActivity: AppScreens("LoginActivity")
    object RegisterActivity: AppScreens("RegisterActivity")
    object MainScreen: AppScreens("MainScreen")
    object FoodScreen: AppScreens("FoodScreen")
    object OrderScreen: AppScreens("OrderScreen")
    object ProfileScreen: AppScreens("ProfileScreen")
    object PaymentScreen: AppScreens("PaymentScreen")
    object ConfirmationScreen: AppScreens("ConfirmationScreen")
    object HistoryScreen: AppScreens("HistoryScreen")
    /*object Delete: AppScreens("Delete")
    object ShowContacts: AppScreens("ShowContacts")*/

}
