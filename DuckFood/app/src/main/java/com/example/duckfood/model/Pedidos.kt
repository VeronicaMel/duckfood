package com.example.duckfood.model

data class Pedidos(
    var email: String = "",
    var importe: Double = 0.0,
    var direccionEntrega: String = "",
    var fecha: String = ""
)
