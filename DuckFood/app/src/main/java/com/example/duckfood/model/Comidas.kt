package com.example.duckfood.model

data class Comidas(
    val nombre: String,
    val categoria: String,
    val descripcion: String,
    val precio: Double,
    val url: String)
