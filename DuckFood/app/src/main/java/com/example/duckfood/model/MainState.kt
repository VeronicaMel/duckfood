package com.example.duckfood.model

data class MainState(
    val isLoading: Boolean = false,
    val comidas: List<Comidas> = emptyList()
)
