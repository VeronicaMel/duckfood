package com.example.duckfood.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.R
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.screens.ui.theme.orange
import com.example.practicafirebase.navigation.AppScreens
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(navController: NavController) {
    LaunchedEffect(key1 = true) {
        delay(2000)
        navController.popBackStack()
        navController.navigate(route = AppScreens.FirstActivity.route)
    }
    Splash(navController)
}

@Composable
fun Splash(navController: NavController){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(fondo),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center

    ) {

        Image(
            painterResource(id = com.example.duckfood.R.drawable.duckfoodv2),
            contentDescription = "Logo de DuckFood",
            modifier = Modifier
                .padding(top = 16.dp, bottom = 24.dp)
                .size(500.dp)
                .padding(16.dp)
                .align(Alignment.CenterHorizontally)
        )

    }
}