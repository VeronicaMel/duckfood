package com.example.duckfood.model

data class Clientes(
    val nombre: String,
    val apellidos: String,
    val direccion: String,
    val email: String,
    val telefono: String)