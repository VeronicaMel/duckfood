package com.example.duckfood.screens.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val orange = Color(0xFFFF5722)
val yellow = Color(0xFFFF9800)
val black = Color(0xFF000000)
val white = Color(0xFFFFFFFF)
val fondo = Color(0xFFFFC77D)