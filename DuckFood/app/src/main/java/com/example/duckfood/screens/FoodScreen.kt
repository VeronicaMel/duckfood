package com.example.duckfood.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.model.Comidas
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.ui.theme.naranja

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun FoodScreen(viewModel: ViewModel, navController: NavHostController) {

    FoodScreenBodyContent(viewModel, navController = navController)

}

@Composable
fun FoodScreenBodyContent(viewModel: ViewModel,navController: NavHostController) {
    var currentFood: LiveData<Comidas> = viewModel.selectedFood
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(fondo)
            .fillMaxSize()

    ) {
        Spacer(modifier = Modifier.size(24.dp))
        Card(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth().fillMaxHeight(0.9f),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color(0xFFFFF2B0)
        ) {

            Column(modifier = Modifier
                .padding(bottom = 24.dp)
                .verticalScroll(rememberScrollState()),
                horizontalAlignment = CenterHorizontally
            )
            {

                FirebaseImg(url = currentFood.value!!.url)
                Spacer(modifier = Modifier.size(24.dp))
                Row(
                    modifier = Modifier
                        .padding(start = 32.dp, end= 32.dp),
                    verticalAlignment = Alignment.CenterVertically
                ){
                    Text(
                        text = currentFood.value!!.nombre,
                        textAlign = TextAlign.Start,
                        color = naranja,
                        fontSize = 25.sp,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier.weight(2f)
                    )
                    Text(
                        text = currentFood.value!!.precio.toString()+"€",
                        textAlign = TextAlign.End,
                        color = naranja,
                        fontSize = 25.sp,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier.weight(1f)
                    )
                }
                Spacer(modifier = Modifier.size(24.dp))
                Text(
                    text = currentFood.value!!.descripcion,
                    modifier = Modifier.padding(start = 32.dp, end= 32.dp),
                    textAlign = TextAlign.Justify,
                    color = naranja,
                    fontWeight = FontWeight.Bold
                )

            }
        }
        Row(modifier = Modifier.fillMaxHeight().fillMaxWidth()){
            Column(modifier = Modifier.fillMaxWidth(0.080f)){}
            Button(onClick = { navController.navigate("MainScreen") },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = naranja  ,
                    contentColor = Color.White,
                ),
                border = BorderStroke(2.dp, color = naranja),
                modifier = Modifier
                    .fillMaxHeight(0.8f)
                    .fillMaxWidth(0.2f)
                    .padding(end = 10.dp),
                shape = RoundedCornerShape(100.dp)){
                Icon(imageVector = Icons.Default.ArrowBack , contentDescription = "Retroceder" )
            }
            Button(onClick = {
                var comida = currentFood.value!!
                viewModel.añadirPedidoActual(comida, navController)
            },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = naranja,
                    contentColor = Color.White,
                ),
                border = BorderStroke(2.dp, color = naranja),
                modifier = Modifier
                    .fillMaxHeight(0.8f)
                    .fillMaxWidth(0.9f),
                shape = RoundedCornerShape(100.dp)){
                Text(text = "Añadir al pedido", fontSize = 25.sp, fontWeight = FontWeight.Bold)
            }
        }
    }
}


@Composable
fun FirebaseImg(url: String) {
    val painter = rememberAsyncImagePainter(url)
    Image(
        painter = painter,
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .size(360.dp, 360.dp)
            .padding(start = 24.dp, end = 24.dp, top = 24.dp)
            .border(width = 2.dp, color = naranja),
        alignment = Center
    )
}








