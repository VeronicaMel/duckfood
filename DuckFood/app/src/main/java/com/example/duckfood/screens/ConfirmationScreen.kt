package com.example.duckfood.screens

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.duckfood.R
import com.example.duckfood.ViewModel.ViewModel
import com.example.duckfood.screens.ui.theme.fondo
import com.example.duckfood.ui.theme.naranja


@Composable
fun ConfirmationScreen(ViewModel: ViewModel, navController: NavHostController) {
    BackHandler {
        // do nothing
    }
    ConfirmationScreenBodyContent(navController = navController, viewModel = ViewModel)
}

@Composable
fun ConfirmationScreenBodyContent(navController: NavHostController, viewModel: ViewModel) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(fondo)
            .fillMaxSize()

    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight(0.25f)){}
        Card(
            modifier = Modifier
                .padding(bottom = 16.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth()
                .fillMaxHeight(0.75f),
            elevation = 16.dp,
            shape = RoundedCornerShape(8.dp),
            backgroundColor = Color(0xFFFFF2B0)
        ) {
            Column(modifier = Modifier.fillMaxSize()) {
                Image(
                        painter = painterResource(id = R.drawable.duckfoodv2),
                        contentDescription = "LogoDuckFood",
                        modifier = Modifier
                            .padding(top = 20.dp)
                            .align(Alignment.CenterHorizontally)
                )
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.5f)) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        Row(modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight(0.33f)){}
                        Row(modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight(0.4f)) {
                            Column(modifier = Modifier.fillMaxWidth(0.17f)){}
                            Text(
                                text = "El pedido está en marcha!!",
                                color = naranja,
                                style = MaterialTheme.typography.h3,
                                modifier = Modifier
                                    .weight(2f)
                                    .padding(start = 10.dp)
                            )
                        }
                        Row(modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()) {
                            Column(modifier = Modifier.fillMaxWidth(0.048f)){}
                            Text(
                                text = "Gracias por confiar en DuckFood",
                                color = naranja,
                                style = MaterialTheme.typography.h2,
                                modifier = Modifier
                                    .weight(2f)
                            )
                        }
                    }
                }
                Row(modifier = Modifier .fillMaxHeight(0.3f)){}
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()) {
                    Column(modifier = Modifier.fillMaxWidth(0.18f)){}
                    Button(
                        onClick = {
                            navController.navigate("MainScreen")
                            viewModel.historial!!.clear()
                            viewModel.searchOrdersInDatabase(viewModel.mail.value)
                        },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = naranja,
                            contentColor = Color.White,
                        ),
                        border = BorderStroke(2.dp, color = naranja),
                        modifier = Modifier.fillMaxHeight(0.6f),
                        shape = RoundedCornerShape(100.dp)
                    ) {
                        Icon(imageVector = Icons.Default.Home, contentDescription = "Volver")
                        Text(
                            text = "Volver al inicio",
                            fontSize = 25.sp,
                            fontWeight = FontWeight.Bold
                        )

                    }
                }
                }
            }
        }
    }






